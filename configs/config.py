import yaml


def _join(loader, node):
    seq = loader.construct_sequence(node)
    return "/".join([str(i) for i in seq])


yaml.add_constructor("!join", _join)


def read_config(cfg_path):
    with open(cfg_path, "r") as f:
        cfg = yaml.load(f, yaml.FullLoader)

    return cfg
