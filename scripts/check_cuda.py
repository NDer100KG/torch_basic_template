from core.utils.console import console
import torch

good = torch.cuda.is_available()
if good:
    console.print("torch OK", style="green")
else:
    console.print("torch FAIL", style="red")
