#!/bin/bash
export PYTHONPATH=${PWD}

RED='\033[0;31m'
RESET='\033[0m'
GREEN='\033[0;32m'

test_function(){
    if $1; then
        echo -e "${GREEN} $1 OK ${RESET}"
    else
        echo -e "${RED} $1 FAIL ${RESET}"
    fi
}