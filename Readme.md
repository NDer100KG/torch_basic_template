# Tensorflow2 Basic Template

TODO List:
- [ ] Model Quantization

## Installation
```
conda env create -f environment.yml
```

## Training
Main Progress
```
python main.py -c configs/sample_config.yaml
```


## Scripts
### Unittests
Test if each main function in every module works fine
```
./scripts/run_all_test.sh
```
<img src="attachments/unittest.png">


## WandB
1. Build local-hosted wandb server, docker is needed
    ```
    pip install wandb
    wandb local
    ```
2. Login to local server
    ```
    wandb login --host=http://localhost:8080
    ```

## Line Notify
1. Generate your own auth code in [line notify](https://notify-bot.line.me/zh_TW/)
2. Paste your auth code in [core/utils/_line_auth.yaml](core/utils/_line_auth.yaml) and rename it to `line_auth.yaml`
3. Your line will notify you when you've done training