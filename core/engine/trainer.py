import numpy as np
import torch.nn as nn
import torch
import os
from rich.progress import Progress

from core.utils.wandb_runner import wandb_runner
from core.utils.console import console
from core.utils.line_notify import line_notifier


def do_train(cfg, model, train_loader, val_loader, optimizer, scheduler, criterion, global_step, checkpointer):
    console.print("Start training", style="bold cyan")

    min_val_loss = 1e10

    with Progress() as progress:
        epoch_pbar = progress.add_task("[red] Epoch", total=cfg["SOLVER"]["EPOCHS"])
        train_pbar = progress.add_task("[green] Train step", total=len(train_loader))
        val_pbar = progress.add_task("[cyan] Val step", total=len(val_loader))

        for epoch in range(cfg["SOLVER"]["EPOCHS"]):
            progress.update(epoch_pbar, advance=1)
            model.train()

            if cfg["MODEL"]["BACKBONE"]["bn_freeze"]:
                modules = model.model.modules()
                for m in modules:
                    if isinstance(m, nn.BatchNorm2d):
                        m.eval()

            for train_iter, train_batch in enumerate(train_loader):
                progress.update(train_pbar, advance=1)

                pred = model(train_batch["image"].cuda())
                loss = criterion(pred, train_batch["label"])

                optimizer.zero_grad()
                loss.backward()

                optimizer.step()

                global_step += 1
                if wandb_runner.run and train_iter % cfg["WANDB"]["INTERVAL"] == 0:
                    wandb_runner.log({"loss": loss.data.cpu().numpy()}, step=global_step)

            scheduler.step()

            _val_loss = []
            with torch.no_grad():
                for val_iter, val_batch in enumerate(val_loader):
                    progress.update(val_pbar, advance=1)

                    pred = model(val_batch["image"].cuda())
                    loss = criterion(pred, val_batch["label"])
                    _val_loss.append(loss.data.cpu().numpy())

            _val_loss = np.mean(_val_loss)

            if wandb_runner.run:
                wandb_runner.log({"val/loss": _val_loss}, step=global_step)

            if _val_loss < min_val_loss:
                print(
                    "Save checkpoints and artifacts, loss decrease from ", min_val_loss, " to ", _val_loss,
                )
                checkpointer.save(cfg["SAVE"]["MODEL_NAME"])

                min_val_loss = _val_loss

            global_step = global_step + 1

            progress.reset(val_pbar)
            progress.reset(train_pbar)

    if line_notifier.headers:
        line_notifier.send("Training Done")
