import os, cv2, sys
import numpy as np
from glob import glob

from torch.utils.data import Dataset


class BaseDataSet(Dataset):
    """
    Basic Dataset read image path from img_source
    img_source: list of img_path and label
    """

    def __init__(self, cfg, is_training=False, debug=False):
        self.img_source = cfg["TRAIN_IMG_SOURCE"] if is_training else cfg["TEST_IMG_SOURCE"]
        assert os.path.exists(self.img_source), f"{self.img_source} NOT found."

        self.cfg = cfg
        self.debug = debug
        self.is_training = is_training

        self.load_data()

    def __len__(self):
        return len(self.all_images)

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return f"| Dataset Info |datasize: {self.__len__()}|"

    def load_data(self):
        self.all_images = glob(self.img_source + "/*.jpg")

    def read_image(self, path):
        img = cv2.imread(path, 1)
        img = cv2.resize(img, (self.cfg["IMAGE_SIZE"][0], self.cfg["IMAGE_SIZE"][1]))

        return img

    def normalize(self, image):
        return image / 255.0

    def __getitem__(self, index):
        img = self.read_image(self.all_images[index]).astype(np.float32)

        if self.debug:
            cv2.imshow("img", img)
            k = cv2.waitKey(0)
            if k == 27:
                sys.exit()

        img = self.normalize(img)
        img = np.transpose(img, (2, 0, 1))

        return {"image": img, "label": 1}
