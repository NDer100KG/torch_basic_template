from torch.utils.data import DataLoader
from core.data.base_dataset import BaseDataSet
from core.data.collate_batch import collate_fn


def build_data(cfg, is_training=False, debug=False):
    data_cfg = cfg["DATA"]
    dataset = BaseDataSet(data_cfg, is_training, debug=debug)

    if is_training:
        data_loader = DataLoader(
            dataset,
            num_workers=data_cfg["NUM_WORKERS"],
            pin_memory=True,
            batch_size=data_cfg["TRAIN_BATCHSIZE"],
            batch_sampler=None,
            shuffle=True,
            drop_last=True,
            collate_fn=collate_fn,
            worker_init_fn=None,
        )
    else:
        data_loader = DataLoader(
            dataset,
            shuffle=False,
            batch_size=data_cfg["TEST_BATCHSIZE"],
            num_workers=data_cfg["NUM_WORKERS"],
            drop_last=True,
            collate_fn=collate_fn,
            worker_init_fn=None,
        )
    return data_loader


if __name__ == "__main__":
    from configs.config import read_config
    from rich.progress import track

    cfg = read_config("configs/sample_config.yaml")

    dataloader = build_data(cfg, is_training=False, debug=False)

    for i in track(range(10)):
        batch = next(iter(dataloader))
        image = batch["image"]
        # print("image shape: ", image.shape)

