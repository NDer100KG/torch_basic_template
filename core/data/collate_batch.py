import torch
import numpy as np


def collate_fn(batch):
    imgs = torch.tensor(np.stack([v["image"] for v in batch]))
    labels = torch.tensor(np.stack([v["label"] for v in batch]), dtype=torch.int64)

    return {"image": imgs, "label": labels}
