from core.modeling.backbone import build_backbone
from core.modeling.heads import build_head
from core.utils.console import console
from core.utils.wandb_runner import wandb_runner

import os
import torch.nn as nn
from torchsummary import summary
from torch.nn.modules import Sequential
from collections import OrderedDict


class Model(nn.Module):
    def __init__(self, cfg):
        super(Model, self).__init__()
        self.backbone = build_backbone(cfg["MODEL"]["BACKBONE"])
        self.head = build_head(cfg["MODEL"]["HEAD"], self.backbone.num_ftrs)

        self.model = Sequential(OrderedDict([("backbone", self.backbone), ("head", self.head)]))

        self.cfg = cfg
        self.model_cfg = cfg["MODEL"]

    def __call__(self, images):
        return self.model(images)

    def GetTrainableParameters(self):
        params = []
        for k, v in self.model.named_parameters():
            if not v.requires_grad:
                continue

            lr_mul = 1.0
            if "backbone" in k:
                lr_mul = 0.1
            params += [{"params": [v], "lr_mul": lr_mul}]

        return params

    def Summary(self):
        summary(self.model.cuda(), (3, cfg["DATA"]["IMAGE_SIZE"][0], cfg["DATA"]["IMAGE_SIZE"][1]))

    def QuantizeModel(self):
        pass

    def SaveFrozenPb(self):
        pass

        if wandb_runner.run:
            wandb_runner.run.save(os.path.join(self.cfg["SAVE"]["MODEL_PATH"], self.cfg["SAVE"]["MODEL_NAME"]))

    def SaveWeights(self, path):
        pass

    def LoadWeights(self, path):
        pass


if __name__ == "__main__":
    from configs.config import read_config

    cfg = read_config("configs/sample_config.yaml")

    model = Model(cfg)
    model.QuantizeModel()

    model.Summary()

    model.SaveFrozenPb()
