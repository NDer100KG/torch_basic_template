import torch
import torch.nn as nn
from core.modeling.registry import BACKBONES

from torchvision.models import mobilenet_v2


@BACKBONES.register("mobilenetv2")
class MobileNetV2(nn.Module):
    def __init__(self, cfg):
        super(MobileNetV2, self).__init__()
        self.model = mobilenet_v2(pretrained=cfg["pretrained"])
        self.num_ftrs = self.model.classifier[1].in_features
        self.cfg = cfg

        if cfg["avg_pool"]:
            self.avg_pool = nn.AdaptiveAvgPool2d((1, 1))
        if cfg["include_top"]:
            self.fc = self.model.classifier

    def forward(self, x):
        x = self.model.features(x)

        if self.cfg["avg_pool"]:
            x = self.avg_pool(x)

        if self.cfg["include_top"]:
            x = self.fc(x)

        return x

