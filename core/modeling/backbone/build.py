from core.modeling.registry import BACKBONES

from core.modeling.backbone.resnet import *
from core.modeling.backbone.mobilenetv2 import *


def build_backbone(cfg):
    assert (
        cfg["NAME"] in BACKBONES
    ), f"backbone {cfg} is not registered in registry : {BACKBONES.keys()}"
    return BACKBONES[cfg["NAME"]](cfg)
