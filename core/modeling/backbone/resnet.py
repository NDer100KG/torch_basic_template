import torch
import torch.nn as nn
import math
import torch.nn.functional as F
from torch.autograd import Variable
import numpy as np
import torch.nn.init as init
from torchvision.models import resnet18
from torchvision.models import resnet34
from torchvision.models import resnet50
from torchvision.models import resnet101
import torch.utils.model_zoo as model_zoo

from core.modeling.registry import BACKBONES


@BACKBONES.register("ResNet18")
class Resnet18(nn.Module):
    def __init__(self, cfg):
        super(Resnet18, self).__init__()

        self.model = resnet18(cfg["pretrained"])
        self.num_ftrs = self.model.fc.in_features
        self.cfg = cfg

        if cfg["avg_pool"]:
            self.avg_pool = self.model.avgpool
        if cfg["include_top"]:
            self.fc = self.model.fc

        if cfg["bn_freeze"]:
            for m in self.model.modules():
                if isinstance(m, nn.BatchNorm2d):
                    m.eval()
                    m.weight.requires_grad_(False)
                    m.bias.requires_grad_(False)

    def forward(self, x):
        x = self.model.conv1(x)
        x = self.model.bn1(x)
        x = self.model.relu(x)
        x = self.model.maxpool(x)
        x = self.model.layer1(x)
        x = self.model.layer2(x)
        x = self.model.layer3(x)
        x = self.model.layer4(x)

        if self.cfg["avg_pool"]:
            x = self.avg_pool(x)

        if self.cfg["include_top"]:
            x = self.fc(x)

        return x


@BACKBONES.register("ResNet34")
class Resnet34(nn.Module):
    def __init__(self, cfg):
        super(Resnet34, self).__init__()

        self.model = resnet34(cfg["pretrained"])
        self.num_ftrs = self.model.fc.in_features

        if cfg["avg_pool"]:
            self.avg_pool = self.model.avgpool
        if cfg["include_top"]:
            self.fc = self.model.fc

        if cfg["bn_freeze"]:
            for m in self.model.modules():
                if isinstance(m, nn.BatchNorm2d):
                    m.eval()
                    m.weight.requires_grad_(False)
                    m.bias.requires_grad_(False)

    def forward(self, x):
        x = self.model.conv1(x)
        x = self.model.bn1(x)
        x = self.model.relu(x)
        x = self.model.maxpool(x)
        x = self.model.layer1(x)
        x = self.model.layer2(x)
        x = self.model.layer3(x)
        x = self.model.layer4(x)

        if self.cfg["avg_pool"]:
            x = self.avg_pool(x)

        if self.cfg["include_top"]:
            x = self.fc(x)

        return x


@BACKBONES.register("ResNet50")
class Resnet50(nn.Module):
    def __init__(self, cfg):
        super(Resnet50, self).__init__()

        self.model = resnet50(cfg["pretrained"])
        self.num_ftrs = self.model.fc.in_features

        if cfg["avg_pool"]:
            self.avg_pool = self.model.avgpool
        if cfg["include_top"]:
            self.fc = self.model.fc

        if cfg["bn_freeze"]:
            for m in self.model.modules():
                if isinstance(m, nn.BatchNorm2d):
                    m.eval()
                    m.weight.requires_grad_(False)
                    m.bias.requires_grad_(False)

    def forward(self, x):
        x = self.model.conv1(x)
        x = self.model.bn1(x)
        x = self.model.relu(x)
        x = self.model.maxpool(x)
        x = self.model.layer1(x)
        x = self.model.layer2(x)
        x = self.model.layer3(x)
        x = self.model.layer4(x)

        if self.cfg["avg_pool"]:
            x = self.avg_pool(x)

        if self.cfg["include_top"]:
            x = self.fc(x)

        return x


@BACKBONES.register("ResNet101")
class Resnet101(nn.Module):
    def __init__(self, cfg):
        super(Resnet101, self).__init__()

        self.model = resnet101(cfg["pretrained"])
        self.num_ftrs = self.model.fc.in_features

        if cfg["avg_pool"]:
            self.avg_pool = self.model.avgpool
        if cfg["include_top"]:
            self.fc = self.model.fc

        if cfg["bn_freeze"]:
            for m in self.model.modules():
                if isinstance(m, nn.BatchNorm2d):
                    m.eval()
                    m.weight.requires_grad_(False)
                    m.bias.requires_grad_(False)

    def forward(self, x):
        x = self.model.conv1(x)
        x = self.model.bn1(x)
        x = self.model.relu(x)
        x = self.model.maxpool(x)
        x = self.model.layer1(x)
        x = self.model.layer2(x)
        x = self.model.layer3(x)
        x = self.model.layer4(x)

        if self.cfg["avg_pool"]:
            x = self.avg_pool(x)

        if self.cfg["include_top"]:
            x = self.fc(x)

        return x
