import torch
from torch import nn

from core.modeling.registry import HEADS


@HEADS.register("linear_norm")
class LinearNorm(nn.Module):
    def __init__(self, cfg, in_channel):
        super(LinearNorm, self).__init__()
        self.fc = nn.Linear(in_channel, cfg["DIM"])

    def forward(self, x):
        x = x.view(x.size(0), -1)
        x = self.fc(x)
        x = nn.functional.normalize(x, p=2, dim=1)
        return x
