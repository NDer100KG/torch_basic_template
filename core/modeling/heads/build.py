from core.modeling.registry import HEADS

from core.modeling.heads.linear_norm import LinearNorm


def build_head(cfg, in_channel):
    assert cfg["NAME"] in HEADS, f"head {cfg['NAME']} is not defined"

    return HEADS[cfg["NAME"]](cfg, in_channel)
