import torch
from torch import nn

from core.losses.registry import LOSS
from pytorch_metric_learning import miners, losses


@LOSS.register("MultiSimilarityLoss")
class MultiSimilarityLoss(torch.nn.Module):
    def __init__(self,):
        super(MultiSimilarityLoss, self).__init__()
        self.thresh = 0.5
        self.epsilon = 0.1
        self.scale_pos = 2
        self.scale_neg = 50

        self.miner = miners.MultiSimilarityMiner(epsilon=self.epsilon)
        self.loss_func = losses.MultiSimilarityLoss(self.scale_pos, self.scale_neg, self.thresh)

    def forward(self, embeddings, labels):
        hard_pairs = self.miner(embeddings, labels)
        loss = self.loss_func(embeddings, labels, hard_pairs)
        return loss


@LOSS.register("ContrastiveLoss")
class ContrastiveLoss(nn.Module):
    def __init__(self, margin=0.5, **kwargs):
        super(ContrastiveLoss, self).__init__()
        self.margin = margin
        self.loss_func = losses.ContrastiveLoss(neg_margin=self.margin)

    def forward(self, embeddings, labels):
        loss = self.loss_func(embeddings, labels)
        return loss


@LOSS.register("TripletLoss")
class TripletLoss(nn.Module):
    def __init__(self, margin=0.1, **kwargs):
        super(TripletLoss, self).__init__()
        self.margin = margin
        self.miner = miners.TripletMarginMiner(margin, type_of_triplets="semihard")
        self.loss_func = losses.TripletMarginLoss(margin=self.margin)

    def forward(self, embeddings, labels):
        hard_pairs = self.miner(embeddings, labels)
        loss = self.loss_func(embeddings, labels, hard_pairs)
        return loss


@LOSS.register("NPairLoss")
class NPairLoss(nn.Module):
    def __init__(self, l2_reg=0):
        super(NPairLoss, self).__init__()
        self.l2_reg = l2_reg
        self.loss_func = losses.NPairsLoss(l2_reg_weight=self.l2_reg, normalize_embeddings=False)

    def forward(self, embeddings, labels):
        loss = self.loss_func(embeddings, labels)
        return loss


@LOSS.register("custom_loss")
class CustomLoss(nn.Module):
    def __init__(self, cfg):
        super(CustomLoss, self).__init__()

    def forward(self, x, y):
        loss = torch.ones(1) * 10
        loss.requires_grad = True
        return loss


@LOSS.register("mse")
class MSE(nn.Module):
    def __init__(self, cfg):
        super(MSE, self).__init__()
        self.mse = nn.MSELoss()

    def forward(self, y, y_hat):
        """
        Args:
            y (float): [N, C]
            y_hat (float): [N, C]

        Returns:
            [float]: mean((y_hat - y) ** 2)
        """
        return self.mse(y, y_hat)


@LOSS.register("l1")
class L1(nn.Module):
    def __init__(self, cfg):
        super(L1, self).__init__()
        self.l1 = nn.L1Loss()

    def forward(self, y, y_hat):
        """

        Args:
            y (float): [N, C]
            y_hat (float): [N, C]

        Returns:
            [float]: mean(|y_hat - y|)
        """
        return self.l1(y, y_hat)


@LOSS.register("CrossEntropy")
class CrossEntropy(nn.Module):
    def __init__(self, cfg):
        super(CrossEntropy, self).__init__()
        self.cross_entropy = nn.CrossEntropyLoss()

    def forward(self, pred, target):
        """

        Args:
            pred (float): [N]
            target (float): [N], class label

        Returns:
            [float]:
        """
        return self.cross_entropy(pred, target)
