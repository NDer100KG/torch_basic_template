from core.losses.losses import *
from core.losses.registry import LOSS


def build_loss(cfg):
    loss_name = cfg["LOSSES"]["NAME"]
    assert loss_name in LOSS, f"loss name {loss_name} is not registered in registry :{LOSS.keys()}"
    return LOSS[loss_name](cfg["LOSSES"][loss_name])


if __name__ == "__main__":
    from configs.config import read_config

    cfg = read_config("configs/sample_config.yaml")

    criterion = build_loss(cfg)

    print(criterion)
