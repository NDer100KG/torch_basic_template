import torch
from core.solver.registry import OPTIMIZER


@OPTIMIZER.register("SGD")
def SGD(params_group, **kwargs):
    return torch.optim.SGD(params_group, **kwargs)


@OPTIMIZER.register("Adam")
def Adam(params_group, **kwargs):
    return torch.optim.Adam(params_group, **kwargs)


@OPTIMIZER.register("rmsprop")
def rmsprop(params_group, **kwargs):
    return torch.optim.rmsprop(params_group, **kwargs)


@OPTIMIZER.register("AdamW")
def AdamW(params_group, **kwargs):
    return torch.optim.AdamW(params_group, **kwargs)

