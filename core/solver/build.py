from core.solver.lr_scheduler import *
from core.solver.optimizer import *
from core.solver.registry import SCHEDULER, OPTIMIZER


def build_optimizer(cfg, model):
    opt_name = cfg["SOLVER"]["OPTIMIZER_NAME"]
    opt_attr = cfg["SOLVER"][opt_name]
    opt_attr = {k: float(v) for k, v in opt_attr.items()}

    params = model.GetTrainableParameters()

    return OPTIMIZER[opt_name](params, **opt_attr)


def build_lr_scheduler(cfg, opt):
    scheduler_name = cfg["SOLVER"]["SCHEDULER"]
    scheduler_attr = cfg["SOLVER"][scheduler_name]

    return SCHEDULER[cfg["SOLVER"]["SCHEDULER"]](opt, **scheduler_attr)
