import wandb


class WandbRunner:
    def __init__(self) -> None:
        self.run = False

    def start(self, cfg):
        self.run = wandb.init(
            config={},
            project=cfg["WANDB"]["PROJECT"],
            name=cfg["WANDB"]["TEST_NAME"],
            group=cfg["WANDB"]["GROUP"],
            tags=cfg["WANDB"]["TAG"],
        )

    def log(self, log_dict, step):
        self.run.log(log_dict, step)


wandb_runner = WandbRunner()

