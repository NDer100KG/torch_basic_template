import requests, sys, os
from configs.config import read_config


class LineNotifier:
    def __init__(self) -> None:
        self.headers = None

    def build(self, line_auth):
        self.headers = read_config(line_auth)

    def send(self, message="test123"):
        params = {"message": message}
        requests.post("https://notify-api.line.me/api/notify", headers=self.headers, params=params)


line_notifier = LineNotifier()

if __name__ == "__main__":
    args = sys.argv

    line_notifier.build("utils/line_auth.yaml")

    line_notifier.send("Hello World")
