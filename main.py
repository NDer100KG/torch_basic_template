import os
import sched

from core.utils.checkpoint import Checkpointer

os.environ["WANDB_IGNORE_GLOBS"] = "upstream_diff*,conda-environment.yaml,requirements.txt,output.log"

import argparse
import torch.nn as nn

from core.engine.trainer import do_train
from core.data.datasets import build_data
from core.losses.build import build_loss
from core.modeling.model import Model
from core.solver.build import (
    build_lr_scheduler,
    build_optimizer,
)
from core.utils.console import console
from core.utils.checkpoint import Checkpointer
from core.utils.wandb_runner import wandb_runner
from core.utils.line_notify import line_notifier


def main(cfg):

    model = Model(cfg).cuda()
    if len(cfg["CUDA_VISIBLE_DEVICES"]) > 1:
        model = nn.DataParalel(model)

    criterion = build_loss(cfg)

    optimizer = build_optimizer(cfg, model)
    scheduler = build_lr_scheduler(cfg, optimizer)

    train_loader = build_data(cfg, is_training=True)
    val_loader = build_data(cfg, is_training=False)

    console.print(train_loader.dataset)
    console.print(val_loader.dataset)

    global_step = 1

    if not os.path.exists(cfg["SAVE"]["MODEL_PATH"]):
        os.makedirs(cfg["SAVE"]["MODEL_PATH"])
    checkpointer = Checkpointer(model, optimizer=optimizer, scheduler=scheduler, save_dir=cfg["SAVE"]["MODEL_PATH"])

    if cfg["WANDB"]["ENABLE"]:
        wandb_runner.start(cfg)

    if cfg["LINE_NOTIFY"]["ENABLE"]:
        line_notifier.build(cfg["LINE_NOTIFY"]["AUTH_PATH"])
    do_train(cfg, model, train_loader, val_loader, optimizer, scheduler, criterion, global_step, checkpointer)


def parse_args():
    """
    Parse input arguments
    """
    parser = argparse.ArgumentParser(description="Train a retrieval network")
    parser.add_argument(
        "-c", "--cfg", dest="cfg_file", help="config file", default="configs/sample_config.yaml", type=str,
    )
    return parser.parse_args()


if __name__ == "__main__":
    from configs.config import read_config

    args = parse_args()
    cfg = read_config(args.cfg_file)

    devices = ""
    for d in cfg["CUDA_VISIBLE_DEVICES"]:
        devices = devices + str(d) + ","
    os.environ["CUDA_VISIBLE_DEVICES"] = devices

    console.print(cfg)

    main(cfg)
